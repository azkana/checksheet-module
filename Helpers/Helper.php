<?php

if(!function_exists('getStatusName')) {
    function getStatusName($code) {
        switch($code) {
            case 'D':
                return 'Draft';
                break;
            case 'W':
                return 'Waiting for Verification';
                break;
            case 'V':
                return 'Verified';
                break;
            case 'R':
                return 'Rejected';
                break;
            default:
                return 'N/A';
                break;
        }
    }
}
