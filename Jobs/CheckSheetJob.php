<?php

namespace Modules\ChecksheetManagement\Jobs;

use App\Models\Logs\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Modules\ChecksheetManagement\Emails\CheckSheetMail;

class CheckSheetJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $config;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new CheckSheetMail($this->config);
        Mail::to($this->config['email'])->send($email);
        $id = $this->config['log_id'];
        if (count(Mail::failures())) {
            // Sending Email Failed
            Email::findOrFail($id)->update([
                'email_status'  => 'F',
                'failed_at'     => date('Y-m-d H:i:s')
            ]);
        } else {
            // Sending Email Successful
            Email::findOrFail($id)->update([
                'email_status'  => 'S',
                'send_at'       => date('Y-m-d H:i:s')
            ]);
        }
    }
}
