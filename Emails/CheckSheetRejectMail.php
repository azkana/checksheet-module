<?php

namespace Modules\ChecksheetManagement\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CheckSheetRejectMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params['css']          = config('beautymail.view.css');
        $params['logo']         = config('beautymail.view.logo');
        $params['senderName']   = config('beautymail.view.senderName');
        $params['unsubscribe']  = null;
        $params['data']         = $this->data;
        return $this->view('checksheetmanagement::emails.checksheet-reject', $params)
            ->subject('Checksheet Rejected - ' . date('d-m-Y'));
    }
}
