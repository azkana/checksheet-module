<?php

namespace Modules\ChecksheetManagement\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Logs\Email;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use Modules\ChecksheetManagement\Entities\ChecksheetDaily;
use Modules\ChecksheetManagement\Jobs\CheckSheetJob;

class ChecksheetDailyController extends Controller
{
    protected $pageTitle;

    public function __construct()
    {
        $this->pageTitle = 'Unit Operation Schedule';
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        return view('checksheetmanagement::admin.checksheet.daily.index', $params);
    }

    public function indexData(Request $request)
    {
        if($request->ajax()) {
            $data = new ChecksheetDaily();
            $data = $data->orderBy('created_at', 'DESC');
            $data = $data->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('attachment', function($row) {
                    switch ($row->filename) {
                        case null:
                            return '<span class="text-danger"><i class="fa fa-times"></i></span>';
                            break;
                        default:
                            return '<span class="text-success"><i class="fa fa-check"></i></span>';
                            break;
                    }
                })
                ->addColumn('status', function($row) {
                    $verifiedBy = $row->status == 'V' ? ' by ' . $row->verifiedBy->name : null;
                    return getStatusName($row->status) . $verifiedBy;
                })
                ->addColumn('action', function($row) {
                    $btnSubmit = in_array($row->status, ["D","R"]) && Auth::user()->hasPermissionTo('checksheet-daily-upload') ? 
                        '<li>
                            <a href="#" onclick="submit(\''.$row->id.'\');return false;"><i class="fa fa-send"></i>Upload</a>
                        </li>' : null;
                    $btnDelete = $row->status == 'D' && Auth::user()->hasPermissionTo('checksheet-daily-delete') ? 
                        '<li>
                            <a href="#" data-toggle="modal" data-target="#delete-'.$row->id.'"><i class="fa fa-trash"></i>Delete</a>
                        </li>' : null;
                    $btnVerify = $row->status == 'W' && Auth::user()->hasPermissionTo('checksheet-daily-approve') ? 
                        '<li>
                            <a href="#" onclick="verify(\''.$row->id.'\');return false;"><i class="fa fa-check"></i>Verify</a>
                        </li>' : null;
                    $btnReject = $row->status == 'W' && Auth::user()->hasPermissionTo('checksheet-weekly-reject') ? 
                        '<li>
                            <a href="#" onclick="reject(\''.$row->id.'\');return false;"><i class="fa fa-times"></i>Reject</a>
                        </li>' : null;
                    $btn = '
                        <div class="btn-group">
                            <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Action</span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li>
                                    <a href="#" onclick="detail(\''.$row->id.'\');return false;"><i class="fa fa-eye"></i>Detail</a>
                                </li>
                                '. $btnSubmit .'
                                '. $btnDelete .'
                                '. $btnVerify .'
                                '. $btnReject .'
                            </ul>
                        </div>
                        <div class="modal fade text-left" id="delete-'.$row->id.'" tabindex="-1" role="dialog" aria-labelledby="delete">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form method="POST" action="'.route("checksheet.daily.destroy", $row->id).'">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            Apakah anda yakin akan menghapus data member <b>'.$row->name.'</b>?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    ';
                    return $btn;
                })
                ->rawColumns([
                    'attachment', 'status', 'action'
                ])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['week'] = Carbon::now();
        return view('checksheetmanagement::admin.checksheet.daily.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $data = new ChecksheetDaily();
            $data->year = $request->year;
            $data->week = $request->week;
            $data->date = dateFormatYmd($request->date);
            $data->name = $request->test;
            $data->note = $request->note;
            $data->status = 'D';
            $data->created_by = Auth::user()->id;

            if($request->hasFile('attc')) {
                $file = $request->file('attc');
                $filename = _upload_file($file, null, 'checksheet/daily/', 'public');

                $data->filename = $filename;
            }

            $data->save();

            return response()->json([
                'message' => 'Data berhasil disimpan.'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    public function submitForm(Request $request) {
        $params['data'] = ChecksheetDaily::findOrFail($request->id);
        $params['week'] = Carbon::now();
        return view('checksheetmanagement::admin.checksheet.daily.submit', $params);
    }

    public function submitSave(Request $request) {
        try {
            $checksheet = ChecksheetDaily::findOrFail($request->id);

            if($request->hasFile('attc')) {
                $file = $request->file('attc');
                $filename = _upload_file($file, null, 'checksheet/daily/', 'public');
            }

            $checksheet->update([
                'filename' => $filename,
                'status' => 'W',
                'uploaded_by' => Auth::user()->id,
                'uploaded_at' => Carbon::now(),
                'updated_by' => Auth::user()->id
            ]);
            return response()->json([
                'message' => 'Upload successfully'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request)
    {
        $params['data'] = $data = ChecksheetDaily::findOrFail($request->id);
        if(!empty($data->filename)) {
            $ext = pathinfo(storage_path('checksheet/daily/' . $data->filename), PATHINFO_EXTENSION);
            $params['ext'] = $ext;
        }
        return view('checksheetmanagement::admin.checksheet.daily.show', $params);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('checksheetmanagement::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $checksheet = ChecksheetDaily::findOrFail($id);
            if(!empty($checksheet->filename)) {
                Storage::disk('public')->delete('checksheet/daily/' . $checksheet->filename);
            }
            $checksheet->delete();
            
            return redirect()->route('checksheet.daily')
                ->with('success', trans('message.success_delete'));
        } catch(\Exception $e) {
            return redirect()->route('checksheet.daily')
                ->with('error', trans('message.error_delete'));
        }
    }

    public function download($id)
    {
        $data = ChecksheetDaily::findOrFail($id);
        return Storage::disk('public')->download('checksheet/daily/' . $data->filename);
    }

    public function reminder()
    {
        $check = ChecksheetDaily::_get_data_reminder();
        if($check) {

            $user = User::where('email_verified_at', '<>', null)
                ->role(['admin', 'supervisor'])
                ->pluck('email');
                
            $dataEmail['full_name']     = 'Supervisor & Admin';
            $dataEmail['email']         = $user;
            $dataEmail['dateSub']       = Carbon::now()->subDay()->format('d-m-Y');
            $logEmail = Email::create([
                'email_from'    => config('mail.from.address'),
                'email_to'      => $dataEmail['email']->implode(','),
                'email_subject' => $this->pageTitle . ' Reminder - ' . date('d-m-Y'),
                'email_status'  => 'Q',
                'module'        => 'checksheet',
                'queue_at'      => date('Y-m-d H:i:s')
            ]);
            $dataEmail['log_id']  = $logEmail->id;

            CheckSheetJob::dispatch($dataEmail)
                        ->delay(now()->addMinutes(0));
        }
    }
}
