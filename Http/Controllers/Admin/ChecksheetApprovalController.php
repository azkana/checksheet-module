<?php

namespace Modules\ChecksheetManagement\Http\Controllers\Admin;

use App\Models\Logs\Email;
use Modules\ChecksheetManagement\Entities\ChecksheetWeekly;
use Modules\ChecksheetManagement\Entities\ChecksheetDaily;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Modules\ChecksheetManagement\Jobs\CheckSheetRejectJob;

class ChecksheetApprovalController extends Controller
{
    protected $pageTitle;

    public function __construct()
    {
        $this->pageTitle = 'Input Schedule';
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        return view('checksheetmanagement::admin.checksheet.approval.index', $params);
    }

    public function indexDataDaily(Request $request)
    {
        if($request->ajax()) {
            $data = new ChecksheetDaily();
            // $data = $data->where('status', 'D');
            $data = $data->orderBy('created_at', 'DESC');
            $data = $data->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function($row) {
                    return getStatusName($row->status);
                })
                ->addColumn('action', function($row) {
                    $btnDelete = $row->status == 'D' && Auth::user()->hasPermissionTo('checksheet-daily-delete') ? 
                        '<li>
                            <a href="#" data-toggle="modal" data-target="#delete-'.$row->id.'"><i class="fa fa-trash"></i>Delete</a>
                        </li>' : null;
                    $btn = '
                        <div class="btn-group">
                            <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Action</span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                '. $btnDelete .'
                            </ul>
                        </div>
                        <div class="modal fade text-left" id="delete-'.$row->id.'" tabindex="-1" role="dialog" aria-labelledby="delete">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form method="POST" action="'.route("checksheet.daily.destroy", $row->id).'">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            Apakah anda yakin akan menghapus data member <b>'.$row->name.'</b>?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    ';
                    if ( in_array($row->status, ['D']) && Auth::user()->hasPermissionTo('checksheet-daily-delete') ) {
                        return $btn;
                    } else {
                        return null;
                    }
                })
                ->rawColumns([
                    'status', 'action'
                ])
                ->make(true);
        }
    }

    public function indexDataWeekly(Request $request)
    {
        if($request->ajax()) {
            $data = new ChecksheetWeekly();
            // $data = $data->where('status', 'W');
            $data = $data->orderBy('created_at', 'DESC');
            $data = $data->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function($row) {
                    return getStatusName($row->status);
                })
                ->addColumn('action', function($row) {
                    $btnDelete = $row->status == 'D' && Auth::user()->hasPermissionTo('checksheet-weekly-delete') ? 
                        '<li>
                            <a href="#" data-toggle="modal" data-target="#delete-'.$row->id.'"><i class="fa fa-trash"></i>Delete</a>
                        </li>' : null;
                    $btn = '
                        <div class="btn-group">
                            <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Action</span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                '. $btnDelete .'
                            </ul>
                        </div>
                        <div class="modal fade text-left" id="delete-'.$row->id.'" tabindex="-1" role="dialog" aria-labelledby="delete">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form method="POST" action="'.route("checksheet.weekly.destroy", $row->id).'">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="'.csrf_token().'">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            Apakah anda yakin akan menghapus data member <b>'.$row->name.'</b>?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    ';
                    if ( in_array($row->status, ['D']) && Auth::user()->hasPermissionTo('checksheet-daily-delete') ) {
                        return $btn;
                    } else {
                        return null;
                    }
                })
                ->rawColumns([
                    'status', 'action'
                ])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function verifyForm(Request $request)
    {
        switch ($request->type) {
            case 'D':
                $checksheet = ChecksheetDaily::findOrFail($request->id);
                break;
            case 'W':
                $checksheet = ChecksheetWeekly::findOrFail($request->id);
                break;
        }
        $params['data'] = $checksheet;
        $params['type'] = $request->type;
        $params['path'] = $path = $request->type == 'D' ? 'daily' : 'weekly';
        if(!empty($checksheet->filename)) {
            $ext = pathinfo(storage_path('checksheet/'. $path .'/' . $checksheet->filename), PATHINFO_EXTENSION);
            $params['ext'] = $ext;
        }
        return view('checksheetmanagement::admin.checksheet.approval.show', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function verifySave(Request $request)
    {
        try {
            switch ($request->type) {
                case 'D':
                    $checksheet = ChecksheetDaily::findOrFail($request->id);
                    break;
                case 'W':
                    $checksheet = ChecksheetWeekly::findOrFail($request->id);
                    break;
            }
            
            $checksheet->update([
                'status' => 'V',
                'verified_by' => Auth::user()->id,
                'verified_at' => Carbon::now()
            ]);
            return response()->json([
                'message' => 'Verified successfully'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    public function rejectForm(Request $request)
    {
        switch ($request->type) {
            case 'D':
                $checksheet = ChecksheetDaily::findOrFail($request->id);
                break;
            case 'W':
                $checksheet = ChecksheetWeekly::findOrFail($request->id);
                break;
        }
        $params['data'] = $checksheet;
        $params['type'] = $request->type;
        $params['path'] = $path = $request->type == 'D' ? 'daily' : 'weekly';
        if(!empty($checksheet->filename)) {
            $ext = pathinfo(storage_path('checksheet/'. $path .'/' . $checksheet->filename), PATHINFO_EXTENSION);
            $params['ext'] = $ext;
        }
        return view('checksheetmanagement::admin.checksheet.approval.reject', $params);
    }

    public function rejectSave(Request $request)
    {
        try {
            switch ($request->type) {
                case 'D':
                    $checksheet = ChecksheetDaily::findOrFail($request->id);
                    break;
                case 'W':
                    $checksheet = ChecksheetWeekly::findOrFail($request->id);
                    break;
            }
            
            $checksheet->update([
                'status' => 'R',
                'verify_note' => $request->note,
                'verified_by' => Auth::user()->id,
                'verified_at' => Carbon::now()
            ]);

            $dataEmail['full_name']     = $checksheet->uploadedBy->name;
            $dataEmail['email']         = $checksheet->uploadedBy->email;

            $logEmail = Email::create([
                'email_from'    => config('mail.from.address'),
                'email_to'      => $dataEmail['email'],
                'email_subject' => $checksheet->name . ' Rejected - ' . date('d-m-Y'),
                'email_status'  => 'Q',
                'module'        => 'checksheet',
                'queue_at'      => date('Y-m-d H:i:s')
            ]);
            $dataEmail['log_id']  = $logEmail->id;

            CheckSheetRejectJob::dispatch($dataEmail)
                        ->delay(now()->addMinutes(0));

            return response()->json([
                'message' => 'Rejected successfully'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
    }
}
