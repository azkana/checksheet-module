<?php

namespace Modules\ChecksheetManagement\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;
use Carbon\Carbon;

class ChecksheetDaily extends Model
{
    use TraitsUuid;

    protected $table = 'checksheet_daily';

    public $incrementing    = false;

    protected $fillable = [
        'name',
        'note',
        'filename',
        'year',
        'week',
        'date',
        'status',
        'uploaded_by',
        'uploaded_at',
        'verified_by',
        'verified_at',
        'verify_note',
        'created_by',
        'updated_by'
    ];

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by')
            ->withDefault([
                'name' => null
            ]);
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by')
            ->withDefault([
                'name' => null
            ]);
    }

    public function uploadedBy()
    {
        return $this->belongsTo(User::class, 'uploaded_by')
            ->withDefault([
                'name' => null
            ]);
    }

    public function verifiedBy()
    {
        return $this->belongsTo(User::class, 'verified_by')
            ->withDefault([
                'name' => null
            ]);
    }

    public static function _get_data_reminder()
    {
        return ChecksheetDaily::where('status', 'D')
            ->where('date', Carbon::now()->subDay()->format('Y-m-d'))
            ->count();
    }
}
