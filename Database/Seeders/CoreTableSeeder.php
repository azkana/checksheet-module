<?php

namespace Modules\ChecksheetManagement\Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            'checksheet',
            'checksheet-daily-list',
            'checksheet-daily-create',
            'checksheet-daily-upload',
            'checksheet-daily-download',
            'checksheet-daily-show',
            'checksheet-daily-edit',
            'checksheet-daily-delete',
            'checksheet-daily-approve',
            'checksheet-daily-reject',
            'checksheet-weekly-list',
            'checksheet-weekly-create',
            'checksheet-weekly-upload',
            'checksheet-weekly-download',
            'checksheet-weekly-show',
            'checksheet-weekly-edit',
            'checksheet-weekly-delete',
            'checksheet-weekly-approve',
            'checksheet-weekly-reject',
        ];

        foreach($permissions as $row) {
            $permission = Permission::where('name', $row)->first();
            if($permission) {
                $this->command->info('Permission ' .$row. ' already exists.');
            } else {
                Permission::create([
                    'name' => $row
                ]);
                $this->command->info('Permission ' .$row. ' created successfully.');
            }
        }

        $roles = ['operator', 'admin', 'engineer', 'supervisor', 'administrator'];

        foreach($roles as $row) {
            $role = Role::where('name', $row)->first();
            if($role) {
                $this->command->info('Role ' .$row. ' already exists.');
                $thisRole = $role;
            } else {
                $newRole = Role::create([
                    'name' => $row
                ]);
                $this->command->info('Role ' .$row. ' created successfully.');
                $thisRole = $newRole;
            }

            switch($row) {
                case 'operator':
                    $thisRole->syncPermissions([
                        'checksheet',
                        'checksheet-daily-list',
                        'checksheet-daily-upload',
                        'checksheet-daily-show',
                        'checksheet-weekly-list',
                        'checksheet-weekly-upload',
                        'checksheet-weekly-show',
                    ]);
                    break;
                case 'admin':
                    $thisRole->syncPermissions([
                        'checksheet',
                        'checksheet-daily-list',
                        'checksheet-daily-create',
                        'checksheet-daily-download',
                        'checksheet-daily-show',
                        'checksheet-daily-edit',
                        'checksheet-daily-delete',
                        'checksheet-daily-approve',
                        'checksheet-daily-reject',
                        'checksheet-weekly-list',
                        'checksheet-weekly-create',
                        'checksheet-weekly-download',
                        'checksheet-weekly-show',
                        'checksheet-weekly-edit',
                        'checksheet-weekly-delete',
                        'checksheet-weekly-approve',
                        'checksheet-weekly-reject',
                    ]);
                    break;
                case 'engineer':
                    $thisRole->syncPermissions([
                        'checksheet',
                        'checksheet-daily-list',
                        'checksheet-daily-show',
                        'checksheet-daily-approve',
                        'checksheet-daily-reject',
                        'checksheet-weekly-list',
                        'checksheet-weekly-show',
                        'checksheet-weekly-approve',
                        'checksheet-weekly-reject',
                    ]);
                    break;
                case 'supervisor':
                    $thisRole->syncPermissions([
                        'checksheet',
                        'checksheet-daily-list',
                        'checksheet-daily-show',
                        'checksheet-weekly-list',
                        'checksheet-weekly-show',
                    ]);
                    break;
                case 'administrator':
                    $thisRole->givePermissionTo($permissions);
                    break;
            }
            $this->command->info('Role ' . $row .' syncronized with permissions successfully.');
        }

        /**
         * Create default user
         */

        $users = [
            ['name' => 'Operator', 'email' => 'operator@demo.com'],
            ['name' => 'Engineer', 'email' => 'engineer@demo.com'],
            ['name' => 'Admin', 'email' => 'admin@demo.com'],
            ['name' => 'Supervisor', 'email' => 'supervisor@demo.com'],
        ];

        foreach($users as $row) {
            $user = User::where('email', $row['email'])->first();
            if($user) {
                $this->command->info('User ' . $row['email'] . ' already exists.');
                $thisUser = $user;
            } else {
                $newUser = User::create([
                    'name' => $row['name'],
                    'email' => $row['email'],
                    'email_verified_at' => Carbon::now(),
                    'password' => Hash::make('secret'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
                $this->command->info('User ' . $row['email'] . ' created successfully.');
                $thisUser = $newUser;
            }

            switch($row['email']) {
                case 'operator@demo.com':
                    $thisUser->assignRole('operator');
                    break;
                case 'engineer@demo.com':
                    $thisUser->assignRole('engineer');
                    break;
                case 'admin@demo.com':
                    $thisUser->assignRole('admin');
                    break;
                case 'supervisor@demo.com':
                    $thisUser->assignRole('supervisor');
                    break;
            }
            $this->command->info('User ' . $row['email'] . ' granted successfully.');
        }

    }
}
