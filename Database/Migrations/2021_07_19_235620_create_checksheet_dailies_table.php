<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecksheetDailiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checksheet_daily', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 255)->nullable();
            $table->string('note', 200)->nullable();
            $table->string('filename', 50)->nullable();
            $table->integer('year')->nullable();
            $table->integer('week')->nullable();
            $table->date('date')->nullable();
            $table->string('status', 5)->default('D');
            $table->integer('uploaded_by')->unsigned()->nullable();
            $table->dateTime('uploaded_at')->nullable();
            $table->integer('verified_by')->unsigned()->nullable();
            $table->dateTime('verified_at')->nullable();
            $table->string('verify_note', 200)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();

            $table->index('name');
            $table->index('filename');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('uploaded_by')->references('id')->on('users');
            $table->foreign('verified_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checksheet_daily');
    }
}
