@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')

    <tr>
        <td class="title">
            Hi, <strong>{!! $data['full_name'] !!}</strong>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph" style="text-align: justify">
            <p>Ada yang belum mengisi checksheet pada tanggal {!! $data['dateSub'] !!}. Abaikan jika sudah.</p><br>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td>
            @include('beautymail::templates.minty.button', ['text' => 'Login Sekarang', 'link' => route('login')])
            <img src="{!! route('email.read', 'id='.$data['log_id']) !!}" width="1" height="1" />
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>

    @include('beautymail::templates.minty.contentEnd')

@stop