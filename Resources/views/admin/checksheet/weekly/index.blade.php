@extends('checksheetmanagement::layouts.master')

@section('content')
    
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        @can('checksheet-weekly-create')
                        <button class="btn btn-sm btn-primary" id="new-weekly">
                            <i class="fa fa-plus"></i> New
                        </button>
                        @endcan
                    </div>
                </div>
                <div class="box-body" style="min-height: 520px">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="grid-weekly" class="table table-bordered table-striped" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 2%">#</th>
                                            <th class="text-center" style="width: 10%">Date</th>
                                            <th class="text-center">Test</th>
                                            <th class="text-center" style="width: 20%">Notes</th>
                                            <th class="text-center" style="width: 10%">Attachment</th>
                                            <th class="text-center" style="width: 15%">Status</th>
                                            <th class="text-center" style="width: 5%"><i class="fa fa-navicon"></i></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="modal-box" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="new-weekly-form">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div id="content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#new-weekly').on('click', function() {
                $.ajax({
                    type: "GET",
                    url: "{!! route('checksheet.weekly.create') !!}",
                    success: function(data) {
                        $('#modal-box').modal('toggle');
                        $('#content').html(data);
                    }
                });
            });
            var table = $('#grid-weekly').DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                PaginationType: "two_button",
                paginate:true,
                deferRender: true,
                keys: false,
                scrollX: true,
                dom: '<"row"<"col-md-4"l><"col-md-4 text-center"B><"col-md-4"f>>' + 'rtip',
                buttons: [
                    {
                        text: '<i class="fa fa-refresh"></i> Refresh',
                        className: "btn-sm",
                        action: function(e, dt, node, config) {
                            dt.ajax.reload();
                        }
                    }
                ],
                ajax: { 
                    url: "{!! route('checksheet.weekly.data') !!}",
                    pages: 10,
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', className: "small", orderable: false, searchable: false},
                    {data: 'date', name: 'date', className: "small"},
                    {data: 'name', name: 'name', className: "small", orderable: false},
                    {data: 'note', name: 'note', className: "small", orderable: false},
                    {data: 'attachment', name: 'attachment', className: "small text-center", orderable: false, searchable: false},
                    {data: 'status', name: 'status', className: "small"},
                    {data: 'action', name: 'action', className: "text-center", orderable: false, searchable: false},
                ],
                columnDefs: [
                    {targets: 1, render: $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD-MM-YYYY')},
                ],
                order: [
                    [1, 'desc']
                ],
                language: {
                    processing: "<p class='text-center'>Loading...</p>"
                },
            });
            $('.dataTables_scrollBody').css('height', '350px');
        });
        function submit(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('checksheet.weekly.submit') !!}",
                data: {id: id},
                success: function(data) {
                    $('#modal-box').modal('toggle');
                    $('#content').html(data);
                }
            });
        }
        function detail(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('checksheet.weekly.show') !!}",
                data: {id: id},
                success: function(data) {
                    $('#modal-box').modal('toggle');
                    $('#content').html(data);
                }
            });
        }

        function verify(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('checksheet.approval.verify') !!}",
                data: {
                    id: id,
                    type: 'W'
                },
                success: function(data) {
                    $('#modal-box').modal('toggle');
                    $('#content').html(data);
                }
            });
        }

        function reject(id) {
            $.ajax({
                type: "GET",
                url: "{!! route('checksheet.approval.reject') !!}",
                data: {
                    id: id,
                    type: 'W'
                },
                success: function(data) {
                    $('#modal-box').modal('toggle');
                    $('#content').html(data);
                }
            });
        }
    </script>
@endsection