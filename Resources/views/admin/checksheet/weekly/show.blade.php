<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="new-weekly-form">Pengecekan Plant Detail - {{ $data->name }}</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-lg-6">
            <table class="table table-bordered">
                <tr>
                    <th style="width: 30%">Name</th>
                    <td>{{ $data->name }}</td>
                </tr>
                <tr>
                    <th>Note</th>
                    <td>{{ $data->note }}</td>
                </tr>
                <tr>
                    <th>Year</th>
                    <td>{{ $data->year }}</td>
                </tr>
                <tr>
                    <th>Week</th>
                    <td>{{ $data->week }}</td>
                </tr>
                <tr>
                    <th>Date</th>
                    <td>{{ dateFormatDmy($data->date) }}</td>
                </tr>
            </table>
        </div>
        <div class="col-lg-6">
            <table class="table table-bordered">
                <tr>
                    <th style="width: 30%">Status</th>
                    <td>{{ getStatusName($data->status) }}</td>
                </tr>
                <tr>
                    <th>Created By</th>
                    <td>
                        {{ isset($data->createdBy->name) ? $data->createdBy->name : null }}
                        <small>({{ $data->created_at }})</small>
                    </td>
                </tr>
                @if(!is_null($data->uploaded_by))
                    <tr>
                        <th>Uploaded By</th>
                        <td>
                            {{ $data->uploadedBy->name }}
                            <small>({{ $data->uploaded_at }})</small>
                        </td>
                    </tr>
                @endif
                @if($data->status == 'V')
                <tr>
                    <th>Verified By</th>
                    <td>
                        {{ isset($data->verifiedBy->name) ? $data->verifiedBy->name : null }}
                        <small>({{ $data->verified_at }})</small>
                    </td>
                </tr>
                @endif
                @if($data->status == 'R')
                <tr>
                    <th>Rejected By</th>
                    <td>
                        {{ isset($data->verifiedBy->name) ? $data->verifiedBy->name : null }}
                        <small>({{ $data->verified_at }})</small><br>
                        <b>Note:</b> {!! $data->verify_note !!}
                    </td>
                </tr>
                @endif
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(isset($ext))
                @if($ext == 'pdf')
                    <embed src="{{ \Storage::disk('public')->url('checksheet/weekly/' . $data->filename) }}#toolbar=0" type="application/pdf" style="width: 100%; height: 400px;" />
                @else
                    <img src="{{ \Storage::disk('public')->url('checksheet/weekly/' . $data->filename) }}" style="width: 100%" />
                @endif
                @can('checksheet-daily-download')
                    <a href="{{ route('checksheet.weekly.download', $data->id) }}" class="btn btn-sm btn-info"><i class="fa fa-download"></i> Download</a>
                @endcan
            @endif
        </div>
    </div>
</div>