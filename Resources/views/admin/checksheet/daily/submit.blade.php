<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="new-daily-form">Unit Operation Schedule </h4>
</div>
<form id="form_submit_checksheet" class="form-horizontal">
@csrf
@method('patch')
<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <input type="hidden" name="id" value="{{ $data->id }}" />
            <div class="form-group">
                <label for="year" class="col-sm-2 control-label">Year <span class="required">*</span></label>
                <div class="col-sm-4">
                    {!! Form::text('year', $data->year, [ 'class' => 'form-control input-sm', 'id' => 'year', 'required', 'disabled']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="week" class="col-sm-2 control-label">Week <span class="required">*</span></label>
                <div class="col-sm-4">
                    {!! Form::text('week', $data->week, [ 'class' => 'form-control input-sm', 'id' => 'week', 'required', 'disabled']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="test" class="col-sm-2 control-label">Test <span class="required">*</span></label>
                <div class="col-sm-9">
                    {!! Form::textarea('test', $data->name, [ 'class' => 'form-control input-sm', 'id' => 'test', 'required', 'rows' => 2, 'disabled']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="note" class="col-sm-2 control-label">Notes</label>
                <div class="col-sm-9">
                    {!! Form::textarea('note', $data->note, [ 'class' => 'form-control input-sm', 'id' => 'note', 'rows' => 3, 'disabled']) !!}
                </div>
            </div>
            @can('checksheet-daily-upload')
                <div class="form-group">
                    <label for="attc" class="col-sm-2 control-label">Attachment</label>
                    <div class="col-sm-9">
                        {!! Form::file('attc', null, [ 'class' => 'form-control', 'id' => 'attc', 'required']) !!}
                    </div>
                </div>
            @endcan
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" id="submit"><i class="fa fa-send"></i> Submit</button>
</div>
</form>

<style>
    .errors {
        color:#FF0000;
    }
</style>

<script>
    $(document).ready(function() {
        $('#year').datepicker({
            viewMode: "years", 
            minViewMode: "years",
            format: 'yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $("#submit").on('click', function(e) {
            $("#form_submit_checksheet").validate({
                errorClass: 'errors small',
                rules: {
                    year: {
                        required: true,
                        digits: true,
                        maxlength: 4,
                        minlength: 4,
                    },
                    week: {
                        required: true,
                        digits: true,
                        maxlength: 2,
                        minlength: 1,
                    },
                    test: {
                        required: true,
                    },
                    attc: {
                        required: true,
                    },
                },
                messages: {
                    year: {
                        required: "harus diisi",
                        digits: "harus angka",
                        maxlength: "harus 4 digit",
                        minlength: "harus 4 digit",
                    },
                    week: {
                        required: "harus diisi",
                        digits: "harus angka",
                        maxlength: "maksimal 2 digit",
                        minlength: "minimal 1 digit",
                    },
                    test: {
                        required: "harus diisi",
                    },
                    attc: {
                        required: "harus diisi",
                    },
                },
                submitHandler: function(form) {
                    var formData = new FormData($(form)[0]);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('checksheet.daily.submit.save') }}",
                        data: formData,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        processData: false,
                        cache: false,
                        success: function(data) {
                            $("#modal-box").modal('hide');
                            toastr.success(data.message);
                            $('#grid-daily').DataTable().ajax.reload(null, false);
                        },
                        error: function(error) {
                            toastr.error(error.message);
                        }
                    });
                }
            });
        });
    });
</script>