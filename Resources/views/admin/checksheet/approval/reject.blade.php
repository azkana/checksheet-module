<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modal-box">
        @switch($type)
            @case('D')
                Unit Operation Schedule 
                @break
            @case('W')
                Pengecekan Plant
                @break
            @default
                {{ null }}
        @endswitch
        - {{ $data->name }}
    </h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-lg-6">
            <table class="table table-bordered">
                <tr>
                    <th>Name</th>
                    <td>{{ $data->name }}</td>
                </tr>
                <tr>
                    <th>Note</th>
                    <td>{{ $data->note }}</td>
                </tr>
                <tr>
                    <th>Year</th>
                    <td>{{ $data->year }}</td>
                </tr>
                <tr>
                    <th>Week</th>
                    <td>{{ $data->week }}</td>
                </tr>
                <tr>
                    <th>Date</th>
                    <td>{{ dateFormatDmy($data->date) }}</td>
                </tr>
            </table>
        </div>
        <div class="col-lg-6">
            <table class="table table-bordered">
                <tr>
                    <th>Status</th>
                    <td>{{ getStatusName($data->status) }}</td>
                </tr>
                <tr>
                    <th>Created By</th>
                    <td>
                        {{ isset($data->createdBy->name) ? $data->createdBy->name : null }}
                        <small>({{ $data->created_at }})</small>
                    </td>
                </tr>
                @if(!is_null($data->uploaded_by))
                    <tr>
                        <th>Uploaded By</th>
                        <td>
                            {{ $data->uploadedBy->name }}
                            <small>({{ $data->uploaded_at }})</small>
                        </td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(isset($ext))
                @if($ext == 'pdf')
                    <embed src="{{ _get_file($data->filename, 'checksheet/' . $path . '/', 'public') }}#toolbar=0" type="application/pdf" style="width: 100%; height: 400px;" />
                @else
                    <img src="{{ _get_file($data->filename, 'checksheet/' . $path . '/', 'public') }}" style="width: 100%" />
                @endif
            @endif
            @can(['checksheet-daily-download','checksheet-weekly-download'])
                <a href="{{ route('checksheet.daily.download', $data->id) }}" class="btn btn-sm btn-info"><i class="fa fa-download"></i> Download</a>
            @endcan
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="note" class="col-sm-2 control-label">Notes</label>
                <div class="col-sm-9">
                    {!! Form::textarea('note', null, [ 'class' => 'form-control input-sm', 'id' => 'note', 'rows' => 3]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-danger" id="submit"><i class="fa fa-times"></i> Reject</button>
</div>

<script>
    $(document).ready(function() {
        $("#submit").on('click', function(e) {
            let id = "{{ $data->id }}";
            let csrf = "{{ csrf_token() }}";
            let type = "{{ $type }}";
            let note = $("#note").val();
            $.ajax({
                type: "POST",
                url: "{{ route('checksheet.approval.reject.save') }}",
                data: {
                    id: id,
                    type: type,
                    note: note,
                    _token: csrf,
                    _method: "patch"
                },
                success: function(data) {
                    $("#modal-box").modal('hide');
                    toastr.success(data.message);
                    $('#grid-daily').DataTable().ajax.reload(null, false);
                    $('#grid-weekly').DataTable().ajax.reload(null, false);
                },
                error: function(error) {
                    toastr.error(error.message);
                }
            });
        });
    });
</script>