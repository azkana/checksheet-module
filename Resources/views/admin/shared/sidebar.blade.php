@can('checksheet')
<li class="treeview @if($adminActiveMenu == 'checksheet') active @endif">
    <a href="#">
        <i class="fa fa-users"></i>
        <span>Checksheet</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            <span class="label label-danger pull-right">
                New
            </span>
        </span>
    </a>
    <ul class="treeview-menu">
        @can('checksheet-daily-list')
        <li class="@if($adminActiveSubMenu == 'daily') active @endif">
            <a href="{!! route('checksheet.daily') !!}">
                <i class="fa fa-circle-o"></i> Unit Operation Schedule
                <span class="pull-right-container"></span>
            </a>
        </li>
        @endcan
        @can('checksheet-weekly-list')
        <li class="@if($adminActiveSubMenu == 'weekly') active @endif">
            <a href="{!! route('checksheet.weekly') !!}">
                <i class="fa fa-circle-o"></i> Jadwal Test & Pengecekan Plant
                <span class="pull-right-container"></span>
            </a>
        </li>
        @endcan
        @can(['checksheet-daily-create','checksheet-weekly-create'])
        <li class="@if($adminActiveSubMenu == 'approval') active @endif">
            <a href="{!! route('checksheet.approval') !!}">
                <i class="fa fa-circle-o"></i> Input Schedule
                <span class="pull-right-container"></span>
            </a>
        </li>
        @endcan
    </ul>
</li>
@endcan