<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
    Route::prefix('manage')->group(function() {
        Route::prefix('checksheet')->group(function() {
            Route::name('checksheet.')->group(function() {

                Route::get('dashboard', 'Admin\DashboardController@index')->name('dashboard');

                Route::get('daily', 'Admin\ChecksheetDailyController@index')->name('daily');
                Route::get('daily/data', 'Admin\ChecksheetDailyController@indexData')->name('daily.data');
                Route::get('daily/create', 'Admin\ChecksheetDailyController@create')->name('daily.create');
                Route::post('daily/create', 'Admin\ChecksheetDailyController@store')->name('daily.store');
                Route::get('daily/show', 'Admin\ChecksheetDailyController@show')->name('daily.show');
                Route::get('daily/submit', 'Admin\ChecksheetDailyController@submitForm')->name('daily.submit');
                Route::patch('daily/submit', 'Admin\ChecksheetDailyController@submitSave')->name('daily.submit.save');
                Route::delete('daily/{id}', 'Admin\ChecksheetDailyController@destroy')->name('daily.destroy');
                Route::get('daily/{id}/download', 'Admin\ChecksheetDailyController@download')->name('daily.download');

                Route::get('weekly', 'Admin\ChecksheetWeeklyController@index')->name('weekly');
                Route::get('weekly/data', 'Admin\ChecksheetWeeklyController@indexData')->name('weekly.data');
                Route::get('weekly/create', 'Admin\ChecksheetWeeklyController@create')->name('weekly.create');
                Route::post('weekly/create', 'Admin\ChecksheetWeeklyController@store')->name('weekly.store');
                Route::get('weekly/show', 'Admin\ChecksheetWeeklyController@show')->name('weekly.show');
                Route::get('weekly/submit', 'Admin\ChecksheetWeeklyController@submitForm')->name('weekly.submit');
                Route::patch('weekly/submit', 'Admin\ChecksheetWeeklyController@submitSave')->name('weekly.submit.save');
                Route::delete('weekly/{id}', 'Admin\ChecksheetWeeklyController@destroy')->name('weekly.destroy');
                Route::get('weekly/{id}/download', 'Admin\ChecksheetWeeklyController@download')->name('weekly.download');

                Route::get('approval', 'Admin\ChecksheetApprovalController@index')->name('approval');
                Route::get('approval/data/daily', 'Admin\ChecksheetApprovalController@indexDataDaily')->name('approval.data.daily');
                Route::get('approval/data/weekly', 'Admin\ChecksheetApprovalController@indexDataWeekly')->name('approval.data.weekly');
                Route::get('approval/verify', 'Admin\ChecksheetApprovalController@verifyForm')->name('approval.verify');
                Route::patch('approval/verify', 'Admin\ChecksheetApprovalController@verifySave')->name('approval.verify.save');
                Route::get('approval/reject', 'Admin\ChecksheetApprovalController@rejectForm')->name('approval.reject');
                Route::patch('approval/reject', 'Admin\ChecksheetApprovalController@rejectSave')->name('approval.reject.save');

            });
        });
    });
});

Route::get('reminder/weekly', 'Admin\ChecksheetWeeklyController@reminder')->name('reminder.weekly');
Route::get('reminder/daily', 'Admin\ChecksheetDailyController@reminder')->name('reminder.daily');

// Route::prefix('checksheetmanagement')->group(function() {
//     Route::get('/', 'ChecksheetManagementController@index');
// });
